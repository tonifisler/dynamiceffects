## Dyanamic Effects ###
Dynamic effects is a replacement for dynamic items. It has been completely rewritten see **New Features** for changes
* Dynamic effects lets you apply item based effects to your character or **targeted tokens**, adding/decreasing attack/damage bonuses, increasing/decreasing maximum hit points, changing alignment, in fact almost any field on the character sheet can be adjusted by dynamiceffects.
* The module is not dnd5e dependent, although there is additional support for dnd5e.
* All of the changes exist in game only, the saved version of the actor remains unchanged, with the excpetion that active effect modifiers are stored in a flag so they can be reapplied when the game reloads.
* Effects are created/edited from the item sheet which has an additional tab, Effects.
* An effect is just an actor field specifier (i.e. Attributes HP Max) a mode +/= (add value or set value) and a value. A value can be any valid value for the field with some additional options.
1. You can specifiy a dice specifier in any numeric field or bonus field. So (Bonuses Melee Weapon Damge) "+" 1d4 would add 1d4 to the melee weapon damage bonus. 
2. You can specify @actorField as a value, so (Attributes HP Max) "+" @abilities.con.mod would add the value of the characters con mod to the max HP for the character.
3. You can simple addition/multiplication formules for the value. So (Attributes HP Max) "+" @abilities.con.mod * @abilities.details.level would add you con mod * level to max HP.
4. For numeric fields the values are computed completely during application. So (Abilities Strength) "+" 1d4 would roll 1d4 and add it to your strength. The dice is rerolled each time the character is updated. @fields are always looked up during the evaluation phase. If you strength mod is 5, (Bonuses Melee Weapon Damage) "=" @abilities.str.mod would set the melee weapon bonus damage to 5.
5. For string fields, in particular bonus fields, the dice modifier is not rolled. So when the string field is used in a roll the dice will be rolled. If you set the melee weapon damage bonus to 1d4, 1d4 will be added every time you roll for damage.
There are 2 types of effects, Passive and Active.
* **Passive Effects** Passive effects apply to the actor that has the item in its inventory and can be triggered in 3 ways. 
1. They can be marked "Always Active" in which case having the item in you inventory is enough to have the effects apply. Think innate ability, class feature cursed item.
2. They can be marked "Active when equipped" applying the passive effects when the item is in the actors inventory and equiped. Think magic item that does not require attunement.
3. No special flags set, the effects will applly if the item is equipped and attuned. Think classic magic item.

* **Active Effects** This is the main new feature of dynamiceffects. Active effects are applied to other tokens, not necessarily the actor that has the item in its inventory. The effects are usually applied to the **targeted** tokens for a player. If the item definition specifies a target/range of self then the effects apply to the token holding the item. Active effects persist, even if the item is discarded, until disabled or the effect expires (see Integration with about-time).

* **Armor** The system applies standard DND rules for calculating armor class based on what armor you have eqipped (for natual armor the armor does not need to be equipped). Shield type items add to the armor class. "Effects" can alter armor class as well.

## New Features ##
* The module examines the system description to decide what fields can be modifed. In essence any field specified in template.json can be modified by dynamieffects. For dnd5e additional fields are created, e.g. ability modifiers and so on. This process is not limited to dnd5e but should work with other systems.
* Because there are so many fields that can be adjusted (240+ for dnd5e) the UI for creating effects has been reworked to provide categories which are dnd5e specific. For some/most of the fields that have defined values (languages, damage immunities and so on) a drop down of possible values is displayed when creating such an effect - dnd5e only.
* If about-time is installed the item is examined for a duration and active effects are removed automatically after that amount of gametime passes. If about-time is not installed the effects have to be removed manually.
* Integration with minor-qol so that active effects are automatically applied to targets when attacking if the attack hit, doing damage, consuming (self only), casting a spell if the spell hit or the saving throw fails.
* If the field to be modified is a string, modifiers will be evaluated at the time the effects is used. E.g. bonuses.mwak.attack is a string and 1d4 will be rolled when an attack is made. Dexterity Modifier is a numeric field and the modifer is calculated at the time the characters is "prepared".
* @lookups are supported for almost all fields and @abilities.dex.mod rather than dynamicitems @data.abilities.dex.mod is the preferred format from now on. Hit points, AC and bonuses are calculated last, so that effects that modify dex, will in turn affect dex modifier will in turn affect AC, or if you modify constitution, this will flow into constituion modifier will flow into HP MAX if you create an effect to add the con mod to HP Max.
* Language localization is currently targeted towards dnd5e. However other systems can be supported.
* Not all fields are sensible candidates to modify with dynamiceffects so you might have to experiment to see what makes sense.

### Effect Specifiers ##
You can modify most fields on the character using dynamic effects. 
* A simple example might be a ring of protection. This is active if equipped and attuned. To create the item create a ring (trinket is a good choice for item type or copy it from the premade items compendium). On the effects tab leave active when equippped unchecked (this is for item that are active when you have equipped them without requiring attunement) and always active unchecked (this is for features/items that are always active - for example improved critical that sets weapon critical threshold to 19 instead of the default 20) and create two passive effects, +1 to all saves and +1 to AC. Once you save the tiem whenever it is both attuned and equipped you will gain the specified benefits to your character.
* The bless spell is another good one to try. Find the bless spell in your spellbook and add active effects of (all attacks +1d4 and all saves +14d) - make sure they have the active flag checked )or copy it from the premade items compendim). You can now apply those effects to targeted characrters. The simplest way to apply the effects is to target some friends and run the function DynamicEffects.activateItem() [perhaps from a macro] and you will be presented with a dialog to select the item effects to apply to the characters. The effects will apply until you remove the effects from the targets by running the function again. However, see the notes below for integration with minor-qol and about-time.
* Passive effects are removed when the item/feat that applied them is deactivated or removed from the inventory. Active effects apply to the character until removed, either by user interaction, e.e DynamicEffects.activateItem(), or timeout if about-time is active. 
* You can edit fields on the character sheet and the module will apply effects to the newly entered values. One caveat however applies for string fields. The module will take whatever is in the field as the base value. So if you have @bonuses.mwak.damage + 1d4 as an applied item specifier and you want to put an extra 3 bonus. You must edit the field to be "just" 3 (i.e. remove the text of the auto applied bonus) then save. The auto bonus will be reapplied. If you just add a 3 to the field, you will get +1d4 +3 +1d4, i.e. the bonus will be applied to the saved field which is now +1d4 +3. For numeric fields there is no need to do this, just type in the newe value.

### Macro Functions ##
The macro functions in dynamicitems have been replaced in dynamiceffects with the following (and you will need to modify any macros you have created):
  ```
  DynamicEffects.effects(false/true)
  ```
  * Display a chat message with all enabled/disabled passive and active effects for the selected token. If the paramater is true a detailed list is provided otherwise just the name of the item that is enabled/disabled. Passive effects (those you get from having the item in your inventory) can be toggled on/off. In addition any "active effects" that have been applied to the actor will be displayed, but these cannot be toggled off/on (yet).
  ```
  DynamicEffects.togglePassive(itemName, itemType)
  ```
  * This will enable/disable passive effects for the selected/current actor/token. itemName is the string name of the item, e.g. "Bless". itemType is the type e.g. "feat" and defaults to match the first item that matches by name. Other options are "feat", "weapon", "spell", "equipment", "consumable", "backpack", "class", "tool", "loot". 
  
  ```
  DynamicEffects.setPassive(itemName, true/false, itemType)
  ```
  * Enable/disable the passives for the specified item. enable=true, disable=false. If itemType is not spcified the first item matching itemName will be used. If itemType is specified the matched item must be of the correct type.

  For passive effects, if the item specifies a duration and about-time is installed the effects will be removed after the duration passes in gametime. This is the only way passive effects can be automatically expired.
  ```
  DynamicEffects.activateItem()
  ```
  * This will present a dialog with a list of all items the character possesses with "active affects". You can activate or deactivate the effects. Remember this applies to targeted tokens. If the spell/feature/weapon/tool is denoted as having a target or range of "self" the effect will be applied to user's controlled token/actor instead. If about-time is installed and active and a duration is present in the item definition the effect will automatically be cancelled after the appropriate amount of gametime.

  ```
  DynamicEffects.applyActive(itemName, activate, itemType)
  ```
  * This is the same as activateItem, but acepts parameters for each of itemName, itemType and wheter to activate/deactivate the item effects to the tageted actors/token. If itemType is not spcified the first item matching itemName will be used. If itemType is specified the matched item must be of the correct type.
  ```
  DynamicEffects.removeAllActiveffects()
  ```
  * This removes all active effects from the selected token. Useful when something has gone wrong. You need to be a GM to run this. Can be a macro.

  ### Integration with minor-qol ###
  When rolling an item (weapon, spell, feature, tool, consumable) active effects are applied to the targeted tokens. If the definition of the item indicates that it is self then effects are applied to self. So, a bless spell when cast will apply the active effects to the targeted tokens. 
  minor-qol uses the foundry targeting system to decide who to apply effects to. If a spell is marked as target self then the effects are applied to the casting/using actor. 
  If you have a feat "sneak attack" then rolling the feat will apply the active effects to the token (i.e. weapon attacks do extra damage). For items with a saving throw active effects are applied if the save fails. Other items apply their effects when applying damage/casting a spell (if no saving throw) or consuming an item (e.g. potion).

  https://gitlab.com/tposney/minor-qol 

 ### Integration with about-time ###
  If the underlying spell, wepaon, feat, consumable etc has a duration then the active effects for that item will be applied to the targeted tokens (or self if the item is specified as self) and after the duration specified in the item the effects will be removed. So a bless spell with a duration of 1 minute will apply the active effect bonuses for 1 minute of gametime. A potion of giant strength will apply the new strength for the duration specified in the potion consumable. There are a few sample items in the premade compendium to get you started.

   https://gitlab.com/tposney/about-time  

### Integration with trigger happy ###
Whilst there is no specific integration with trigger happy adding active effect items to trigger tokens allows traps etc to apply effects to characters when they walk onto the trap.

https://github.com/kakaroto/fvtt-module-trigger-happy  

### Sample Items/Macros ###
  There is a compendium of items that have been setup to work with dynamiceffects. All active effects work best if both minor-qol and about-time are installed and active, but will still funciton without them. There is a compendium of sample macros included as well.


## Migrating from dynamiciems ##
Disable dynamicitems/enable dynamiceffects.
from the console or macro run:
```
DynamicEffects.migrateAll(true, true)
```
The first parrameter determines if migration actions are comitted to the database. false means only the in memory version is adjusted. However, if you then make changes to the actor/item the changes will be commited for that actor. It is redcommended that if you run the migration without saving changes, you inspect the actors/items to confirm things are ok then reload and run it the first paramater set to true.
* The second paramater, if true, provides console output indicating all of the changes made.
* The migration will update all world actors and world items, but does not adjust any compendium entries. You will need to import those to your world and then run the migration to get them updated.
* You can run migrateAll multiple times without creating problems.
* Once the migration has been run with the first paramater set to true has been run, there is no going back to dynamicitems as the old data is removed from the character. So if you think you might want to go back **PLEASE MAKE A BACKUP OF YOUR WORLD**. In fact make a backup even if you think you wont go back.

This module makes dynamicitems obsolete and dyanmicitems will not receive further delopment.

### Bugs ###
* If the effect duration is specificed in turns it will be converted to round's worth of seconds, rather than checking the combat tracker. (this will be improved in a subsequent release)
* If the effect duration is specified in rounds it is converted to an equivalent number of seconds. (using the about-time value if there is one, otherwise 6 seconds per round). If about-time is installed this will be correct since about-time advances the gametime clock by the correct number of seconds each round.
* For this release durations are best expressed as times.
* Instantaneous effects are not properly supported and will instead apply for 1 round. A future release will make these permanent on the actor/token.
* This is the first release of dynamiceffects and, whilst it has been inflicted on my players without serious incident, you may well have problems. **PLEASE** backup you world before playing with this module.

### Roadmap ###
The next items to be included will be (in no particular ordeer)
* Spell effects that require concentration to automatically cancel if the casters loses concentration.
* Support for condition application from dynamiceffects
* Support for advantage/disadvantage for specific rolls will require minor-qol.
* Token effects so that you can apply specific token changes, e.g. improved vision, blindness and so on.
* Integration with GM notes so effects can attach GM only info to characters.
* Support for effects that last until end of/start of this/next turn.

## For module/system writers ##
Dynamiceffects overrides the actor.preparedata calls to apply effects. All of the effects are in memeory and not comitted to the database. When applying effects actor.getRollData is used to generate the context against which modifications are applied. The order of application is 
1. Base values (i.e. values that users can modify from the character sheet)
2. normal actor prepareData to calculate first derived effects, e.g. ability modifiers
3. dereived effects for dynamiceffects which include all fields not present in template.json but present in a normal character.
4. The final pass adjusts hp, ac and string saving/roll throw bonuses, like mwak etc.

If a target field is numeric the entirety of the bonus is evaluated in the appropriate pass. So abilities strength.mod + 1d4 will roll the 1d4 during the preparation phase since there is nowhere to store the intermediate state of +1d4. If the target field is a string, e.g. bonuses.mwak.attack + 1d4 the 1d4 is evalulated when the weapon attack is rolled and the bonus field will appear as +1d4 on the character sheet. @values are resolved during the apporpriate pass, so if a character has a dex mod of 4 and you have bonuses.mwak.attack + @abilities.dex.mod, the mwak field will display +4 when examined from the character sheet.

To adapt dynamiceffects to other systems the function ModSpec.createValidMods needs to be adapted to the target system. In addition the dynamiceffectsSelector class wil require modification. However, dynamiceffects should work out of the box with other systems, but not be as useful as it might otherwise be. The integration with about-time is not system specific so should work out of the box. The integration with minor-qol is very dnd5e specific.

The GMAction component is a tool for requesting GMs to execute functions on players behalf. It can be used standalone in other modules without problem.

The module is written in typescript using the procject creator tool from @NickEast and many thanks to him for providing such a good tool.
