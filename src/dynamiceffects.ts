/**
 * This is your TypeScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your module, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your module
 */

// Import TypeScript modules
import { registerSettings } from './module/settings.js';
import { preloadTemplates } from './module/preloadTemplates';
import { dynamiceffectsSetupActions, doEffects, dynamiceffectsInitActions} from "./module/dynamiceffects";
import { dynamiceffectsReadyActions} from "./module/dynamiceffects";
import { dynamiceffectsShowEffects, activateItem, removeActiveEffectsToken, dynamiceffectsTogglePassiveEffect, dynamiceffectsSetPassiveEffect, toggleActorIdEffect, applyActive, setTokenVisibility } from "./module/dynamiceffectsMacros"
import { teleportToToken } from "./module/dynamiceffectsMacros"
import { getItemActiveEffects } from "./module/dynamiceffects"
import { initSheetTab} from "./module/dynamicEffectsTab"
import { ModSpec} from "./module/dynamiceffects"
import { ItemEffectSelector } from './module/apps/dynamiceffectsSelector';
import { migrateItems, migrateActors, migrateAll, migrateAllAts } from './module/migration';
import { GMAction, requestGMAction, GMActionMessage } from './module/GMAction';
import { dynamicEffectsConditionsSetupActions } from './module/dynamicEffectsConditions.js';

/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async function() {
	// Register custom module settings
	registerSettings();
	console.log('dynamiceffects | Initializing dynamiceffects');
	dynamiceffectsInitActions();
	ItemEffectSelector.initActions()

	// Assign custom classes and constants here
	
	// Preload Handlebars templates
	await preloadTemplates();

	// Register custom sheets (if any)
});

/* ------------------------------------ */
/* Setup module							*/
/* ------------------------------------ */
Hooks.once('setup', function() {
	// Do anything after initialization but before
	// ready
	GMAction.initActions
	dynamiceffectsSetupActions();
	ItemEffectSelector.setupActions();
	dynamicEffectsConditionsSetupActions();

  //@ts-ignore
  window.allSpecs = ModSpec.allSpecs;
  //@ts-ignore
	window.allSpecsObj = ModSpec.allSpecsObj;
	//@ts-ignore
	window.DynamicEffects = {
		effects: dynamiceffectsShowEffects,
		togglePassive: dynamiceffectsTogglePassiveEffect,
		setPassive: dynamiceffectsSetPassiveEffect,
		requestGMAction: requestGMAction,
		getItemActiveEffects: getItemActiveEffects,
		removeAllActiveffects: removeActiveEffectsToken,
		_toggleActorIdEffect: toggleActorIdEffect,
		applyActive: applyActive,
		activateItem: activateItem,
		doEffects: doEffects,
		GMAction: GMAction,
		GMActionMessage: GMActionMessage,
		migrateItems: migrateItems,
		migrateActors: migrateActors,
		migrateAll: migrateAll,
		migrateAllAts: migrateAllAts,
		teleportToToken: teleportToToken,
		setTokenVisibility: setTokenVisibility
	}
});

/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function() {
	initSheetTab();

	GMAction.readyActions();
	dynamiceffectsReadyActions();
	ItemEffectSelector.readyActions(); 
});

// Add any additional hooks if necessary
