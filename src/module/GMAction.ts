import {  
  activateItemEffectsForActor, 
  activateItemEffectsForToken, 
  activateItemEffectsForTargets, 
  removeAllItemActiveEffectsTokenId, 
  removeAllItemActiveEffectsActorId, 
  removeAllItemActiveEffectsTargets
} from "./dynamiceffects";

export class GMActionMessage {
  action: string;
  sender: string;
  targetGM: string; // gm id
  data: {};
  constructor(action: string, sender: string, targetGM: string, data: {})
  {
    this.action = action;
    this.sender = sender;
    this.targetGM = targetGM;
    this.data = data;
  }
}


export let requestGMAction = async (action: string, data: {}, debugLog: boolean = false) => {
  if (game.user.isGM) {
    //@ts-ignore
    return await DynamicEffects.GMAction.processAction(action, duplicate(data));
  }
  //@ts-ignore
  let intendedGM = game.users.entities.find(u => u.isGM && u.active);
  //@ts-ignore
  // if (!game.user.isTrusted) return;
  if (!intendedGM) {
    ui.notifications.error(`${game.user.name} ${game.i18n.localize("dynamiceffects.noGM")}`);
    console.error("dynamiceffects | No GM user connected - cannot do request ", action, data);
    return;
  }
  //@ts-ignore
  let message = new DynamicEffects.GMActionMessage(action, game.user.name, intendedGM.id, data);
  if (debugLog) console.log("About to send message", this._moduleSocket, message)
  //@ts-ignore
  game.socket.emit(DynamicEffects.GMAction._moduleSocket, message, resp => {
    if (debugLog) console.log("message sent")
   });
}

export class GMAction {
  public static actions = {
    test: "testMessage",
    actorUpdate: "updateActor",
    activateItemEffectsForActor: "activateItemEffectsForActor",
    activateItemEffectsForToken:  "activateItemEffectsForToken",
    activateItemEffectsForTargets: "activateItemEffectsForTargets",
    removeAllItemActiveEffectsActorId: "removeAllItemActiveEffectsActorId",
    removeAllItemActiveEffectsTokenId: "removeAllItemActiveEffectsTokenId",
    removeAllItemActiveEffectsTargets: "removeAllItemActiveEffectsTargets",
    setTokenVisibility: "setTokenVisibility",
    recreateToken: "recreateToken"

  }

  private static _moduleSocket = "module.dynamiceffects";

  private static _setupSocket() {
    //@ts-ignore
    game.socket.on(this._moduleSocket, (message: GMActionMessage) => {
      if (game.user.id !== message.targetGM) return;
      if (!game.user.isGM) return;
      this.processAction(message.action, message.data)
    });
  }

  public static initActions() {
  }

  public static setupActions () {
  }

  public static readyActions() {
    this._setupSocket();
    requestGMAction("testMessage", game.user.name);
  }

  static chatEffects = (userId: string, actorId: string, itemData, tokenList: string[], flavor: string) => {
    let names = tokenList.map(tid=> canvas.tokens.get(tid).name);
    ChatMessage.create({
      user: game.users.get(userId),
      speaker: {actor: game.actors.get(actorId)},
      content: `${flavor} (${itemData.name}): ${names}`,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      flags: { }
  });
  }

  public static async processAction(action:string, data: {}) {
    var actorId:string;
    //@ts-ignore
    let itemData = data.itemData;
    //@ts-ignore
    var tokenId = data.tokenId;
    var targetList;
    //@ts-ignore
    var requester = data.userId;
    //@ts-ignore
    var actorId = data.actorId;
    switch (action) {
      case "testMessage":
        console.log("test message received", data)
        break;
      case "updateActor":
        //@ts-ignore
        let updateData = data.data;
        let actor = game.actors.get(actorId);
        // not allowed at present - need to find a safe way to do this.
        if (actor) {
          await actor.update(updateData, {})
        }
        break;
      case this.actions.setTokenVisibility:
        //@ts-ignore
        await setTokenVisibility(requester, data)
        break;
      case "activateItemEffectsForTargets":
        //@ts-ignore
        targetList = data.targetList;
        await activateItemEffectsForTargets(targetList, itemData)
        this.chatEffects(requester, actorId, itemData, targetList, "Applying effects")
        break
      case this.actions.activateItemEffectsForToken:
        //@ts-ignore
        tokenId = data.tokenId;
        await activateItemEffectsForToken(tokenId, itemData)
        this.chatEffects(requester, actorId, itemData, [tokenId], "Applying effects")
        break;
      case this.actions.activateItemEffectsForActor:
        //@ts-ignore
        await activateItemEffectsForActor(data.actorId, data.itemData)
        break;
      case this.actions.removeAllItemActiveEffectsTokenId:
        //@ts-ignore
         tokenId = data.tokenId
        await removeAllItemActiveEffectsTokenId(tokenId, itemData);
        this.chatEffects(requester, actorId, itemData, [tokenId], "Removing effects")
        break;
      case this.actions.removeAllItemActiveEffectsActorId:
        //@ts-ignore
        await removeAllItemActiveEffectsActorId(data.actorId, data.itemData);
        break;
      case this.actions.removeAllItemActiveEffectsTargets:
        //@ts-ignore
        targetList = data.targetList;
        await removeAllItemActiveEffectsTargets(targetList, itemData)
        this.chatEffects(requester, actorId, itemData, targetList, "Removing effects")
        break;
      case this.actions.recreateToken:
        //@ts-ignore
        await recreateToken(requester, data);
        break;
      default:
        console.warn("dynamiceffects invalid message received", action, data)
    }
  }
}

// delete a token from the specified scene and recreate it on the target scene.
let recreateToken = async (userId: string, data: {startSceneId: string, targetSceneId: string, tokenData: {}, targetTokenData: {}, x: number, y: number}) => {
  //@ts-ignore
  let scenes = game.scenes;
  let startScene = scenes.get(data.startSceneId);
  let targetScene = scenes.get(data.targetSceneId);
  await targetScene.createEmbeddedEntity('Token', mergeObject(duplicate(data.tokenData), {"x": data.x, "y": data.y, hidden: false}, {overwrite: true, inplace:true}))
  //@ts-ignore
  await startScene.deleteEmbeddedEntity("Token", data.tokenData._id);
}

//Set the hidden status for a token.
let setTokenVisibility = async(userId: string, data: {targetSceneId: string, tokenId: string, hidden: boolean}) => {
  if (!data.targetSceneId || !data.tokenId) return;
  console.log(data.targetSceneId,  data.tokenId, data.hidden)
  //@ts-ignore
  let scene = game.scenes.get(data.targetSceneId);
  console.log(scene)
  return await scene.updateEmbeddedEntity("Token", {"_id": data.tokenId, "hidden": data.hidden})
}
