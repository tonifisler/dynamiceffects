import {mergeObjectPlusLookup} from "./utils"
import { requestGMAction, GMAction } from "./GMAction.js";


var basePreprateData;

export let _characterSpec: {data: any, flags: any} = {data: {}, flags: {}}
export const EVALPASSES = {
  BASESET: 0,
  BASEADD: 1,
  PREPAREDATA : 2,
  DERIVEDSET: 4,
  DERIVEDADD: 5,
  FINALSET: 6,
  FINALADD: 7

}
let templates = {};

var secPerRound;
export var aboutTimeInstalled = false;
export var requireItemTarget = true;

let validateDuration = (duration) => {
  if (!duration.units) duration.units = "minutes";
  if (duration.units.length > 0 && duration.units[duration.units.length -1] !== "s") duration.units = `${duration.units}s`;
  if (!["hours", "minutes", "seconds", "years", "months", "days"].includes(duration.units)) duration.units = "rounds"; 
  if (isNaN(duration.value) || duration.value < 0) duration.value = 1;
  return duration;
}
export var durationUnitsMap: {} = {
  "inst": duration => {return {"seconds": 1}},
  "turn": duration => { return {"turn": (duration.value || 0) * secPerRound}},
  "round": duration => { return {"seconds": (duration.value || 0) * secPerRound}},
  "default": duration => {let val = {};  duration = validateDuration(duration); val[duration.units] = duration.value || 0; return val}
};

let debugLog = false;
let acAffectingArmorTypes;

function debug(...args) {
  if (debugLog) {
    console.log(...args);
  }
}

export class ModSpec {
  static allSpecs: ModSpec[] = [];
  static allSpecsObj: {} = {};
  private static baseSpecs: ModSpec[] = [];
  private static derivedSpecs: ModSpec[] = []

  static specFor(specId: string) {return this.allSpecsObj[specId]}

  constructor(field: string, label: string, pass: number, sampleValue: any = 0) {
    this.field = field;
    this.label = label;
    this.pass = pass;
    this.sampleValue = sampleValue;
  }

  field: string = "";
  label: string = "";
  pass: number = 0;
  mode: string = ""; // "" -> overwrite no eval, += -> nmeric/string/array append add @= -> character lookup + eval
  sampleValue: any = 0;
  targetIds: string[] = null; // does the affect only apply against specific targets

  static createValidMods(characterSpec: {} = game.system.model.Actor.character) {
    _characterSpec["data"] = duplicate(characterSpec);
    let baseValues = flattenObject(_characterSpec);
    if (game.system.id === "dnd5e") { // patch for missing fields
      // data.bonuses.spell.dc hoping will appear in 0.8.3
      baseValues["data.bonuses.spell.dc"] = 0;
      // dynamiceffects pseudo field
      baseValues["data.traits.languages.all"] = false;
      // dynamiceffects psuedo field 
      baseValues["data.bonuses.All-Attacks"] = false;
      baseValues["data.attributes.advantage"] = "none"; //TODO implement this in minor-qol
    }
    // baseSpecs are all those fields defined in template.json game.system.model and are things the user can directly change
    this.baseSpecs = Object.keys(baseValues).map(spec=> new ModSpec(spec, spec, EVALPASSES.BASESET, baseValues[spec]));
  
    // Do the system specific part
    if (game.system.id == "dnd5e") {
      // 1. abilities add mod and save to each;
      Object.keys(_characterSpec.data.abilities).forEach(ablKey => {
        let abl = _characterSpec.data.abilities[ablKey];
        abl.mod = 0; abl.save = 0; abl.min=0
      })
      // adjust specs for bonuses - these are strings, @fields are looked up but dice are not rolled.
      this.baseSpecs = this.baseSpecs.map(
        spec=> {
          if (spec.field.includes("data.bonuses.")){
            spec.pass = EVALPASSES.FINALSET;
        }
        return spec
      })
      // Skills add mod, passive and bonus fields
      Object.keys(_characterSpec.data.skills).forEach(sklKey => {
        let skl = _characterSpec.data.skills[sklKey];
        skl.mod = 0; skl.passive = 0; skl.bonus =0
      })

      // Add special fields
      // 1st group are flags that are absent in model
      // 2nd special fields check_all
      let characterFlags = CONFIG.DND5E.characterFlags; // look at using this to update the _characterSpec instead of hard coding
      mergeObject(_characterSpec, {
        "flags.dnd5e.initiativeAdv": false, 
        "flags.dnd5e.initiativeAlert": false, 
        "flags.dnd5e.initiativeHalfProf": false, 
        "flags.dnd5e.powerfulBuild": false, 
        "flags.dnd5e.savageAttacks": false, 
        "flags.dnd5e.elvenAccuracy": false, 
        "flags.dnd5e.halflingLucky": false, 
        "flags.dnd5e.check_all": 0, 
        "flags.dnd5e.weaponCriticalThreshold": 20,
        "data.attributes.ac.value":  0,
        "data.attributes.ac.min":  0,
        "data.attributes.init.mod":  0,
        "data.attributes.init.total":  0,
        "data.attributes.init.prof":  0,
        "data.attributes.hd":  0,
        "data.attributes.prof":  0,
        "data.attributes.spelldc":  0,
        "data.attributes.spelllevel":  0,
      }, {inplace: true, insertKeys: true, insertValues: true, overwrite: false});
    }
  
    let allSpecsTemp = flattenObject(_characterSpec)
    this.derivedSpecs = Object.keys(allSpecsTemp)
            .filter(specName => {return undefined === this.baseSpecs.find(vs => vs.field === specName)})
            .map(spec=>{return new ModSpec(spec, spec, EVALPASSES.DERIVEDSET, this.allSpecs[spec])})
    this.allSpecs = this.baseSpecs.concat(this.derivedSpecs);
    // Special case for armor/hp which can depend on derived attributes - like dexterity mod or constituion mod
    this.allSpecs.forEach(m=> {if (m.field === "data.attributes.ac.value") m.pass = EVALPASSES.FINALSET})
    this.allSpecs.forEach(m=> {if (m.field === "data.attributes.hp.max") m.pass = EVALPASSES.FINALSET})
    this.allSpecs.forEach(ms => this.allSpecsObj[ms.field] = ms);
  }

  static localizeSpecs() {
    this.allSpecs = this.allSpecs.map(m=> {
      m.label = m.label.replace("data.", "").replace("dnd5e.", "").replace(".value", "").split(".").map(str=>game.i18n.localize(`dynamiceffects.${str}`)).join(" "); 
      return m
    });
  }
}

export class EffectModifier {
  modSpecKey: string;
  value: any;
  mode: string; 
  
  get modSpec() {return ModSpec.specFor[this.modSpecKey]}

  constructor(modSpecKey: string, mode:string, value: any) {
    this.modSpecKey = modSpecKey;
    this.value = value;
    this.mode = mode;
    return this;
  }
}

export function asMergeItem(mod: EffectModifier): {} {
  let modSpec = ModSpec.specFor(mod.modSpecKey);
  if (mod.mode === "=") {
    var pass = modSpec.pass;
    var spec = modSpec.field;
  } else {
    var pass = modSpec.pass + 1; // do additions after assignments
    let specParts = modSpec.field.split(".");
    specParts[specParts.length -1] = `+${specParts[specParts.length - 1]}`;
    var spec = specParts.join(".");
  }
  let item = {};
  item[`${spec}`] = mod.value;
  return item;
}

export class ItemEffect extends EffectModifier {
  id : number;
  itemId: string;
  active: boolean;
  targetSpecific: boolean = false;
  _targets: string[]; // list of token ids that this effect applies against
  get targets () {return this._targets}
  set targets(targets: string[]) {this._targets = targets}

  constructor(id:number, itemId:string = "", modSpecKey: string = ModSpec.allSpecs[0].field, mode:string = "=", value: any = "", active: boolean = false, targetSpecific: boolean = false) {
    super(modSpecKey, mode, value)
    if (debugLog) console.warn("creating item effect with id", id, typeof id)
    this.id = Number(id);
    this.itemId = itemId;
    this.active = active;
    this.targetSpecific = targetSpecific;
    this._targets = [];
  }
}

let uidForActor = (actor:Actor): string => {
  let uid;
  if (!actor.isToken) uid = actor.id;
  else uid = `${actor.token.id}+${actor.id}`;
  return uid;
}

export class TimedItemEffectModifier extends ItemEffect {
  _duration: {value: number, units: string};
  get durartion(): {value: number, units: string} {return this._duration}
  set duration(duration: {value: number, units: string}) {this._duration = duration}
  _startTime: number;

  constructor(id: number, itemId: string, modSpecKey: string, mode:string, value: any, duration: {value: number, units: string} = {value:0, units: "seconds"}, active: boolean = false, targetSpecific: boolean = false) {
    super(id, itemId, modSpecKey, mode, value, targetSpecific);

    this._duration = validateDuration(duration);
    if (aboutTimeInstalled) {
      //@ts-ignore
      this._startTime = Gametime.DTNow().toSeconds();
    } else {
        this._startTime = Date.now();
    }
  }
}

class ActorDataCache {
  static actorDataCache = {};
  static preparedActorDataCache = {};
  static getSavedData(actor: Actor, itemEffects: EffectModifier[]) {
    let uid = uidForActor(actor);
    if (!ActorDataCache.actorDataCache[uid]) {
      if (debugLog) console.log(`Creating new saved data for ${actor.data.name}`, actor.data.data, actor.data.items);

      let savedActorData = {data: {}, flags: {}, itemEffects: []};
      savedActorData.data = duplicate(actor.data.data);
      savedActorData.flags = duplicate(actor.data.flags);
      this.actorDataCache[uid] = savedActorData;
    } else {
      if (debugLog) console.log("restoring saved data for ", actor.name)
      let preparedData = this.preparedActorDataCache[uid];
      // See what has changed in the background since the last prepare data.
      // Chnages will only be things entered for the character not dynamiceffects chagnes so we need to record those
      //@ts-ignore
      let updatesData = diffObject(preparedData.data, actor.data.data);
      //@ts-ignore
      let updatesFlags = diffObject(preparedData.flags, actor.data.flags)
      if (debugLog) {
        console.log("updates since last save", duplicate(updatesData))
        console.log("update flags", updatesFlags)
      }
      mergeObject(this.actorDataCache[uid].flags, updatesFlags, {insertKeys: false, insertValues: false, inplace: true, overwrite: true});
      mergeObject(this.actorDataCache[uid].data, updatesData, {inplace:true, insertKeys: false, insertValues: false, overwrite: true});

      // update the saved actor.data.data with the updates, then set the actor data to the saved data
      // actor.data.data = duplicate(mergeObject(this.actorDataCache[uid].data, updatesData, {inplace:true, insertKeys: false, insertValues: false, overwrite: true}));
      // update flags with changes, then actor data flags
      actor.data.data = duplicate(this.actorDataCache[uid].data);
      actor.data.flags = duplicate(this.actorDataCache[uid].flags);
    }
  }

  static postPrepareData(actor) {
    let uid = uidForActor(actor);
    // save a copy of all that we calculdated so we can check changes next time through.
    this.preparedActorDataCache[uid] = {data: duplicate(actor.data.data), flags: duplicate(actor.data.flags), itemEffects: []};
  }

  permanentChange(actor:Actor, change: {}) {
  }
}

let addArmorEffect = (itemData, effectList, actor) => {
  // Special case for armor
  if (itemData.data.hasOwnProperty("armor") && hasProperty(actor.data, "data.abilities.dex")
    && acAffectingArmorTypes.includes(itemData.data.armor.type) 
    && (itemData.data.equipped || itemData.data.armor.type === "natural")
  ) {
    let acValue = itemData.data.armor.value || 0;
    if (!["shield", "bonus"].includes(itemData.data.armor.type)) {
      let dexMod = itemData.data.armor.dex === 0 ? 0 : actor.data.data.abilities.dex.mod || 0;
      acValue = acValue + (itemData.data.armor.dex === null ? dexMod : Math.min(itemData.data.armor.dex, dexMod));
    }
    if (itemData.data.armor.value !== 0) effectList.push(new EffectModifier("data.attributes.ac.value", ["shield", "bonus"].includes(itemData.data.armor.type) ? "+" : "=", acValue))
  }
}

export let hasItemActiveEffects = (itemData: ItemData):boolean => {
  return (getProperty(itemData, "flags.dynamiceffects.effects") || []).some(effect => effect.active && !effect.targetSpecific);
}
// active effects do not activate when the item is equipped/cursed - they must be applied
export let getItemActiveEffects = (itemData : ItemData): TimedItemEffectModifier[] => {
  // let itemData = item.data;
  // use the item specified duration to set the duration for the effect
  //let duration = itemData.data.duration.value ? {value: itemData.data.duration.value, units: itemData.data.duration.units} : {value: 1, units: "round"};

  // constructing TimedItemEffectModifier tags the start time as "now"
  let effects = (getProperty(itemData, "flags.dynamiceffects.effects") || []).filter(effect => { return effect.active && !effect.targetSpecific});
  if (game.system.id === "dnd5e") {
    effects = effects.reduce((effectList, effect) => effectList.concat(expandSpecial(effect)), [])
  }
  effects = effects.map(ed=> new TimedItemEffectModifier(1, itemData._id, ed.modSpecKey, ed.mode, ed.value, itemData.data.duration, true, false));
  return effects;
}

export let getItemActiveTargetedEffects = (itemData : ItemData): TimedItemEffectModifier[] => {
  // let itemData = item.data;
  // use the item specified duration to set the duration for the effect
  //let duration = itemData.data.duration.value ? {value: itemData.data.duration.value, units: itemData.data.duration.units} : {value: 1, units: "round"};

  // constructing TimedItemEffectModifier tags the start time as "now"
  let effects = (getProperty(itemData, "flags.dynamiceffects.effects") || []).filter(effect => { return effect.active && effect.targetSpecific});
  if (game.system.id === "dnd5e") {
    effects = effects.reduce((effectList, effect) => effectList.concat(expandSpecial(effect)), [])
  }
  effects = effects.map(ed=> new TimedItemEffectModifier(1, itemData._id, ed.modSpecKey, ed.mode, ed.value, itemData.data.duration, true, true));
  return effects;
}

export let itemHasPassiveEffects = (item: Item): boolean => {
  return getItemPassiveEffects(item.data).length > 0;
}
// Get active or passive effects for an item.
export let getItemPassiveEffects = (itemData): EffectModifier[] => {
  return (getProperty(itemData.flags, "dynamiceffects.effects") || [])
      .filter(em=> !em.active && (em.targetIds || []).length === 0)
      .map(em => new EffectModifier(em.modSpecKey||"data.abilities.str.value", em.mode||"+", em.value||0));
}

// get passive item effects for items that are active (equipped/always active etc)
let getAllItemPassiveEffects = (actor: Actor, active: boolean = false, itemId:string = "", ): EffectModifier[] => {
  var itemEffects = actor.data.items
    .filter(itemData =>  hasProperty(itemData, "flags.dynamiceffects.effects") || hasProperty(itemData, "data.armor"))
    .reduce( (effectList, itemData) => {
      addArmorEffect(itemData, effectList,  actor);
      if (hasProperty(itemData, "flags.dynamiceffects.effects") && isActive(itemData)) {
        effectList = effectList.concat(getItemPassiveEffects(itemData));
      }
      return effectList;
    }, []); 

  // see if there are any special effects to process - these are dnd5e specific.
  if (game.system.id === "dnd5e") {
    itemEffects = itemEffects.reduce((effectList, effect) => effectList.concat(expandSpecial(effect)), [])
  }

  if (debugLog) console.log("For actor effects are ", actor.name, itemEffects)
  return itemEffects;
}

// get actor active effects i.e. active effects that have been applied to the actor
let getActiveActorEffects = (actor: Actor): EffectModifier[] => {
  return (getProperty(actor.data.flags, "dynamiceffects.activeEffects") || []).map(em=> 
    new EffectModifier(em.modSpecKey||"data.abilities.str.value", em.mode||"+", em.value||0))
}

export let activateItemEffectsForTargets = async (targetIdList: string[], itemData: ItemData) => {
  return activateEffectsForTargets(targetIdList, getItemActiveEffects(itemData));
}

export let activateItemEffectsForToken = async (token: any, itemData: ItemData) => {
  if (typeof token === "string") token = canvas.tokens.get(token);
  return activateEffectsForActor(token.actor, getItemActiveEffects(itemData))
}

export let activateItemEffectsForActor = async (actor: any, itemData: ItemData) => {
  return activateEffectsForActor(actor, getItemActiveEffects(itemData))
}

export let activateEffectsForActor = async (actor: any, effectListData: any[]) => {
  if (typeof actor === "string") {
    actor = game.actors.get(actor);
  }
  if (actor) {
    let actorEffects = (getProperty(actor.data.flags, "dynamiceffects.activeEffects") || []).concat(effectListData);
    return actor.update({"flags.dynamiceffects.activeEffects": actorEffects});
  }
  return null;
}

// add a list of active effects to the actor
export let activateEffectsForTargets = async (targetIdList: string[], effectListData: any[]) => {
  let promises = [];
  (targetIdList || []).forEach((tId) => {
    let token = canvas.tokens.get(tId)
    if (token && token.actor) {
      let update = activateEffectsForActor(token.actor, effectListData)
      if (update) promises.push(update);
    }
  });
  Promise.all(promises);
  return promises;  
}

// remove all actor effects from the given actor
export let removeAllActorIdEffects = async (actorId) => {
  let actor = game.actors.get(actorId);
  if (actor) return await actor.update({"flags.dynamiceffects.activeEffects": []}, {});
  return null;
}

export let removeAllTokenEffects = async (token) => {
  if (token.actor) return await token.actor.update({"flags.dynamiceffects.activeEffects": []}, {});
  return null;
}

export let removeAllTokenIdEffects = async (tokenId) => {
  let token = canvas.tokens.get(tokenId);
  if (token.actor) return await token.actor.update({"flags.dynamiceffects.activeEffects": []}, {});
  return null;
}
let checkExpiredActions = (combat: Combat) => {
 return;
 console.log("check active combat is ", combat, combat.current, combat.previous)
}

export let removeAllItemActiveEffectsActorId = async (actorId: string, itemData: ItemData) => {
  let actor = game.actors.get(actorId);
  return await removeAllItemActiveEffectsActor(actor, itemData);
}

export let removeAllItemActiveEffectsTargets = async (targetList: string[], itemData: ItemData) => {
  targetList.forEach(tokenId => removeAllItemActiveEffectsTokenId(tokenId, itemData));
}
export let removeAllItemActiveEffectsTokenId = async (tokenId: string, itemData: ItemData) => {
  let token = canvas.tokens.get(tokenId);
  return await removeAllItemActiveEffectsActor(token.actor, itemData);
}

export let removeAllItemActiveEffectsActor = async (actor: Actor, itemData: ItemData) => {
  let activeEffects = getProperty(actor.data, "flags.dynamiceffects.activeEffects") || [];
  //@ts-ignore
  activeEffects = activeEffects.filter(tem => tem.itemId !== itemData._id);
  return await actor.update({"flags.dynamiceffects.activeEffects": activeEffects}, {});
}

// The workhorse that applies all passive and actor effects to the actor
class DynamicEffectsPatching extends Actor {
  prepareData() {
   // let itemEffects = getAllItemPassiveEffects(this, false); // get passive effects for items that are active
    let allEffects = getAllItemPassiveEffects(this, false).concat(getActiveActorEffects(this));
    ActorDataCache.getSavedData(this, allEffects)

    if (debugLog) console.log("effects are ", allEffects);
    Object.keys(EVALPASSES).forEach(k => {
      let updatedContext = false;
      let context = {};
      let pass = EVALPASSES[k];
      if (debugLog) console.log('Doing pass', k)
      if (pass === EVALPASSES.PREPAREDATA) {
        basePreprateData.bind(this)(); 
      } else allEffects.forEach((effect) => {
        // addition mods go one pass later.
        let effectPass = ModSpec.allSpecsObj[effect.modSpecKey].pass + (effect.mode === "+" ? 1 : 0);
        if (pass === effectPass) {
          if (!updatedContext) {
            //@ts-ignore
            context = this.getRollData();
            updatedContext = true;
          }

          //@ts-ignore
          mergeObjectPlusLookup(this.data, asMerge(effect), {inplace: true, insertKeys: true, insertValues: true, context: context, debug: false});
        }
      })
    })
    ActorDataCache.postPrepareData(this); // record the current state of the actor after effects applied
    return this;
  }
}

let asMerge = (mod: EffectModifier): {} => {
  let modSpec = ModSpec.specFor(mod.modSpecKey);
  if (mod.mode === "=") {
    var pass = modSpec.pass;
    var spec = modSpec.field;
  } else {
    let specParts = modSpec.field.split(".");
    specParts[specParts.length -1] = `+${specParts[specParts.length - 1]}`;
    var spec = specParts.join(".");
  }
  let item = {};
  item[`${spec}`] = mod.value;
  return item;
}

// Is the item active? i.e. armor, or equiped or always active
export let isActive = (itemData) => {
  // This test is to allow natural armor effects to be active even if no other flag set.
  if (itemData.data.hasOwnProperty("armor.type") && itemData.data.armor.type === "natural") return true; 
  if (!itemData.flags.hasOwnProperty("dynamiceffects")) return false;
  if (getProperty(itemData, "flags.dynamiceffects.alwaysActive")) return true;
  if (hasProperty(itemData.data, "equipped") && !itemData.data.equipped) return false; 
  if (getProperty(itemData.data, "attuned") || getProperty(itemData, "flags.dynamiceffects.equipActive")) return true;
  return false;
}

// a few dnd specific extensions
function expandSpecial(effect: EffectModifier): EffectModifier[] {
  let checkList = [];
  switch(effect.modSpecKey) {
      case "data.traits.languages.all":
        return [new EffectModifier("data.traits.languages.value", "=", Object.keys(CONFIG.DND5E.languages))];
        // return [new ItemEffect(0, "All Languages", "data.traits.languages.value", "=", Object.keys(CONFIG.DND5E.languages), "Array", "baseEffect")];
      case "skills.all":
          return Object.keys(CONFIG.DND5E.skills).map(skillId  => 
            new EffectModifier(`data.skills.${skillId}.mod`, effect.mode, effect.value));
          return checkList;
      case "data.bonuses.All-Attacks":
        return ["data.bonuses.mwak.attack", "data.bonuses.rwak.attack", "data.bonuses.msak.attack", "data.bonuses.rsak.attack"].map(spec =>
          new EffectModifier(spec, "+", effect.value))
    default:
      return [effect];
  }
}

// replace the standard Actor.prepareData with our prepareData - for dnd5e this is Actor5e.prepareData
function setupProxy() {
  basePreprateData = CONFIG.Actor.entityClass.prototype.prepareData;
  let test = new Proxy( CONFIG.Actor.entityClass.prototype.prepareData, {
    apply: (target, thisvalue, args) =>
    DynamicEffectsPatching.prototype.prepareData.bind(thisvalue)(...args)
  })
  CONFIG.Actor.entityClass.prototype.prepareData = test;
}

export function dynamiceffectsInitActions() {
  setupProxy();
  ModSpec.createValidMods();
}

export function dynamiceffectsSetupActions() {
  if (game.system.id === "dnd5e") {
    acAffectingArmorTypes = Object.keys(CONFIG.DND5E.equipmentTypes).filter(acType => !["trinket"].includes(acType));
  }
}

export function fetchParams() {
  requireItemTarget = game.settings.get("dynamiceffects", "requireItemTarget");
}

export function dynamiceffectsReadyActions() {
  ModSpec.localizeSpecs();
  fetchParams();
  //@ts-ignore
  aboutTimeInstalled = game.modules.get("about-time") && game.modules.get("about-time").active;
  if (aboutTimeInstalled) secPerRound = game.settings.get("about-time", "seconds-per-round") || 6;
  else secPerRound = 6;
  Hooks.on("updateCombat", combatHandler);
  Hooks.on("deleteCombat", deleteCombatHandler);
}

let combats = {};

let combatHandler = (combat: any, updateData: any, otherData:Object, userId:String) => {
  return;
  combat[combat.id] = combat;
  combats[combat.id] = {current: combat.current, previous: combat.previous, turns: combat.turns};
  checkExpiredActions(combats[combat.id])
}

let deleteCombatHandler =  (combat: Combat, id: string, options: any) => {
  delete combats[combat.id];
}

export let doEffects = (item, actor, activate, targets = undefined) => {
  let itemData = item.data;

  if (!hasItemActiveEffects(itemData)) return;
  let targetIds = [];

  if (game.system.id === "dnd5e") {
    if (requireItemTarget && !item.hasTarget) {
      // cannot target anyone
      ui.notifications.warn(`${game.i18n.localize("dynamiceffects.notTargetItem")}`)
      console.log("DynamicEffects | Item does not have targets");
      return;
    }
    if (itemData.data.target && itemData.data.target.type === "self") { // spell effects us
      const speaker = ChatMessage.getSpeaker();
      let token = canvas.tokens.get(speaker.token);
      if (!token) {
        console.log("can't work out who self is");
        return;
      }
      targetIds = [token.id];
    } else { // we need to ]get the targets
      //@ts-ignore
      if (!targets) targets = game.user.targets;
      for (let target of targets) targetIds.push(target.id);
    }
  } else { // Just use the targeted set
    //@ts-ignore
    if (!targets) targets = game.user.targets;
    for (let target of targets) {
      targetIds.push(target.id)
    }
  }

  let action = activate ? GMAction.actions.activateItemEffectsForTargets : GMAction.actions.removeAllItemActiveEffectsTargets;

  requestGMAction(action, {userId: game.user.id, actorId: actor.id, targetList: targetIds, itemData: item.data});
  if (aboutTimeInstalled && activate && item.data.data.duration) {
    let durationMap = durationUnitsMap[item.data.data.duration.units] || durationUnitsMap["default"];
    // console.log("mapped duration is ", durationMap(item.data.data.duration));
    let duration = durationMap(item.data.data.duration);

    //@ts-ignore
    game.Gametime.doIn(duration, DynamicEffects.requestGMAction, DynamicEffects.GMAction.actions.removeAllItemActiveEffectsTargets, {userId: game.user.id, actorId: actor.id, targetList: targetIds, itemData: item.data} )
  } else console.log(`DynamicEffects | no duration specified for ${item.name} effect removal NOT scheduled`)
}