import { requestGMAction, GMAction } from "./GMAction";
import { durationUnitsMap, aboutTimeInstalled, isActive, ModSpec, getItemPassiveEffects, doEffects, removeAllTokenEffects } from "./dynamiceffects";
import { ActiveItemSelector } from "./apps/ActiveitemSelector";

export let applyActive = (itemName: string, activate: boolean = true, itemType: string = "") => {
  var actor: Actor;
  if (canvas.tokens.controlledTokens.length > 0) {
    actor = canvas.tokens.controlledTokens[0].actor;
  }
  if (!actor)
    actor = game.actors.get(ChatMessage.getSpeaker().actor);
  if (!actor) {
    ui.notifications.warn(`${game.i18n.localize("dynamiceffects.noSelection")}`)
    return;
  } 
  let item = actor.items.find(i=> i.name === itemName && (i.type === "" || i.type === itemType));
  if (!item) {
    console.log("No such item ", itemName, actor.items);
    return;
  }
  doEffects(item, actor, activate);
}

export let activateItem = () => {
  //@ts-ignore cant do anything if there are no targets
  const speaker = ChatMessage.getSpeaker();
  const token = canvas.tokens.get(speaker.token);
  if (!token) {
    ui.notifications.warn(`${game.i18n.localize("dynamiceffects.noSelection")}`)
    return;
  } 
  return new ActiveItemSelector(token.actor, {}).render(true);
}

let effectNameString = function(actor, item, active) {
  let btnStyling = 'width: 22px; height:22px; font-size:10px;line-height:1px';
  btnStyling = "width: 60px; padding:0px; line-height:1px;";
  if (!active) btnStyling = btnStyling.concat("font-weight: bold; color: green");
  let activeString = active ? `${game.i18n.localize("dynamiceffects.disable")}` 
                            : `${game.i18n.localize("dynamiceffects.enable")}`;
  let buttonID = `${item.name}`;
  let btntxt = `<button type="button" id="[${item.name}][${item.type}]" style="${btnStyling}">${activeString}</button>`;
  return `<div class="dynamiceffects-toggle dynamiceffects-${active ? "active":"inactive"}-button">${item.name}: ${btntxt}</div>`
  //               : `<div class="dynamiceffects-toggle id="${item.name}" dynamiceffects-inactive-button">${item.name}: ${btntxt}</div>`;
}

let createEffectString = (actor, detailed) => {
  let effectString = "";
  for (let item of actor.items) {
    if (item.data.flags.dynamiceffects && item.data.flags.dynamiceffects.effects) {
      let itemEffects = getItemPassiveEffects(item.data);
      if (itemEffects.length > 0) {
        let active = isActive(item.data);
        effectString = effectString.concat(effectNameString(actor, item, active))
        if (detailed) {
          for (let effect of itemEffects) {
            let effectLabel = ModSpec.allSpecsObj[effect.modSpecKey].label;
            let effectLine = `<div class="dynamiceffects-${active ? 'active' : 'inactive'}-itemeffect">${game.i18n.localize(effectLabel)} ${effect.mode === "=" ? "=" : ""} ${effect.value}</div>`;
            effectString = effectString.concat(effectLine);
          }
        }
      }
    }
  };
  effectString = effectString.concat(game.i18n.localize("dynamiceffects.actoreffects"));

 (getProperty(actor.data.flags, "dynamiceffects.activeEffects")|| []).forEach(ae=> {
    let effectLabel = ModSpec.allSpecsObj[ae.modSpecKey].label;

    let effectLine = `<div class="dynamiceffects-active-itemeffect">${game.i18n.localize(effectLabel)} ${ae.mode === "=" ? "=" : ""} ${ae.value} [${ae._duration.value} ${ae._duration.units}]</div>`;
    effectString = effectString.concat(effectLine);
  })
  if (effectString.length === 0) effectString = "No Effects";
  return effectString;
}

export let dynamiceffectsShowEffects = (detailed: boolean = true) => {
  const speaker = ChatMessage.getSpeaker();
  const token = canvas.tokens.get(speaker.token);
  if (!token) {
    ui.notifications.warn(`${game.i18n.localize("dynamiceffects.noSelection")}`)
    return;
  }
  return effectsActor(token.actor, detailed);
}

let effectsActor = (actor: Actor, detailed = true) => {
  if (!actor) return;
  let flavor = `${"Effect List"}`;
  let effectString = createEffectString(actor, detailed);

  ChatMessage.create({
    user: game.user.id,
    speaker: {actor: actor, alias: actor.name},
    content: effectString,
    whisper: [game.user.id], // ChatMessage.getWhisperIDs("GM"),
    type: CONST.CHAT_MESSAGE_TYPES.OTHER,
    flags: {dynamiceffectsEffects: true, detailed: detailed}
  });
}

let activateHandler = async (message, html, data) => {
  if (!getProperty(message, "data.flags.dynamiceffectsEffects")) return;
  let buttons = html.find(".dynamiceffects-toggle");
  for (let i = 0; i < buttons.length; i++) {
    let button = buttons[i];
    const speaker = message.data.speaker;
    const actor = game.actors.get(speaker.actor);
    const matches = button.children[0].id.match(/\[([^\]]*)\]\[([^\]]*)\]/);
    if (!matches) return;
    const itemName = matches[1];
    const itemType = matches[2];
    button.addEventListener("click", async (ev) => {
      ev.stopPropagation();
      await toggleActorIdEffect(actor.id, itemName, itemType); // have to await so that we geet updated status
      let messageString = createEffectString(actor, getProperty(message, "data.flags.detailed"))
      message.update({"_id": message._id, "content": messageString})
    });
  };
}

Hooks.on("renderChatMessage", activateHandler);

export let dynamiceffectsSetPassiveEffect = (itemName: string, setValue = false, itemType: string = "")  => {
  const speaker = ChatMessage.getSpeaker();
  const token = canvas.tokens.get(speaker.token);
  if (!token) {
    ui.notifications.warn(`${game.i18n.localize("dynamiceffects.noSelection")}`)
    return;
  }
  return toggleActorEffect(token.actor, itemName, itemType, setValue)
}

export let dynamiceffectsTogglePassiveEffect = (itemName: string, itemType: string = "")  => {
  const speaker = ChatMessage.getSpeaker();
  const token = canvas.tokens.get(speaker.token);
  if (!token) {
    ui.notifications.warn(`${game.i18n.localize("dynamiceffects.noSelection")}`)
    return;
  }
  return toggleActorEffect(token.actor, itemName, itemType, undefined)
}

/*
export let toggleTokenEffect = async (tokenId: string, itemName: string, itemType: String, overRide: boolean = undefined) => {
  if (!tokenId) return false;
  let token = canvas.tokens.get(tokenId);
  return toggleActorEffect(token.actor, itemName, itemType, overRide);
}
*/
export let toggleActorIdEffect = async (actorId: string, itemName: string, itemType: String = "", overRide: boolean = undefined) => {
  return toggleActorEffect(game.actors.get(actorId), itemName, itemType, overRide);
}

let toggleActorEffect = async (actor: Actor, itemName: string, itemType: String, overRide: boolean = undefined) => {
  if (!actor) return false;
  let consumeCharge = game.settings.get("dynamiceffects", "ConsumeCharge");
  const item = actor ? actor.items.find(i => i.name === itemName && (itemType === "" || itemType === i.data.type)) : null;
  if ( !item ) {
    ui.notifications.warn(`${game.i18n.localize("dynamiceffects.noeffect")} ${itemName}`);
    return false;
  }

  let useUpdate = {};
  let consumeUpdate = {};
  let newStatus;
  if (item.data.type === "feat") {
    if (!hasProperty(item.data, "flags.dynamiceffects.alwaysActive")) {
      console.log(`Dynamic Effects | item ${item.name} has no alwaysActive property can't toggle`)
      return false;
    }
    newStatus = !item.data.flags.dynamiceffects.alwaysActive;
    if (overRide !== undefined) {
      newStatus = overRide;
    }
    useUpdate = {"_id": item.id, "flags.dynamiceffects.alwaysActive": newStatus};
  } else { // equip/unequip
    newStatus = !item.data.data.equipped;
    if (overRide !== undefined) {
      newStatus = overRide;
    }
    useUpdate = {"_id": item.id, "data.equipped": newStatus};
  }
  if (consumeCharge && newStatus && item.data.data.uses && item.data.data.uses.max > 0) {
    let oldCharges = item.data.data.uses.value;
    if (oldCharges < 1) {
      if (newStatus) { // no charges and inactive cant make it active
        ui.notifications.info(`${actor.name} ${item.name} ${game.i18n.localize("dynamiceffects.noCharges")}`)
        return false;
      }
    }
    consumeUpdate = {"data.uses.value": oldCharges - 1, "_id": item.id}
  }
  ui.notifications.info(`${actor.name} ${item.name} ${game.i18n.localize(newStatus ? "dynamiceffects.active"  : "dynamiceffects.inactive")}`)
  let update = mergeObject(useUpdate, consumeUpdate);
  await actor.updateEmbeddedEntity("OwnedItem", update);
  let duration = item.data.data.duration;
  if (aboutTimeInstalled && newStatus && duration && duration.units) { //only if about-time installed
    // let secPerRound = game.settings.get("about-time", "seconds-per-round");
    let durationMap = durationUnitsMap[duration.units] || durationUnitsMap["default"];
    //@ts-ignore
    game.Gametime.doIn(durationMap(duration), window.DynamicEffects._toggleActorIdEffect, actor.id, itemName, item.type, false)
  }
  return newStatus;
}

export let removeActiveEffectsToken = () => {
  const speaker = ChatMessage.getSpeaker();
  const token = canvas.tokens.get(speaker.token);
  if (token && game.user.isGM) removeAllTokenEffects(token);
}

let tokenScene = (tokenName) => {
  //@ts-ignore
  for (let scene of game.scenes.entities) {
    let token = scene.data.tokens.find(t=> t.name === tokenName);
    if (token) {
      return {scene, token};
    }
  }
  return null;
}

export let teleportToToken = async (token, targetTokenName, xGridOffset: number = 0, yGridOffset: number = 0) => {
  let target = tokenScene(targetTokenName);
  if (!token) {
    console.log("Token not found");
    return("Token not found")
  }
  if (!target) {
    console.log("Target Not found");
    return `Token ${targetTokenName} not found`;
  }
  return teleport(token, target.scene, target.token.x + xGridOffset * canvas.scene.data.grid, target.token.y + yGridOffset * canvas.scene.data.grid)
}

export let teleport = async (token, targetScene, xpos, ypos) => {
  let x = parseInt(xpos);
  let y = parseInt(ypos);

  if (isNaN(x) || isNaN(y)) {
    console.log("Invalid co-ords", xpos, ypos);
    return `Invalid target co-ordinates (${xpos}, ${ypos})`;
  }

  if (!token) {
    console.warn("No Token");
    return "No active token"
  }

  // Hide the current token
  if (targetScene.name === canvas.scene.name) {
    //@ts-ignore
    CanvasAnimation.terminateAnimation( `Token.${token.id}.animateMovement`);
    let sourceSceneId = canvas.scene.id;
    requestGMAction(GMAction.actions.recreateToken, {userId: game.user.id, startSceneId: sourceSceneId, targetSceneId: targetScene.id, tokenData: token.data, x: xpos, y: ypos});
    canvas.pan({x: xpos, y: ypos});
    return true;
  }
  // deletes and recreates the token
  var sourceSceneId = canvas.scene.id;
  Hooks.once("canvasReady", async () => {
    await requestGMAction(GMAction.actions.recreateToken, {userId: game.user.id, startSceneId: sourceSceneId, targetSceneId: targetScene.id, tokenData: token.data, x: xpos, y: ypos});
    canvas.pan({x: xpos, y: ypos});
  })

  // Need to stop animation since we are going to delete the token and if that happens before the animation completes we get an error
  //@ts-ignore
  CanvasAnimation.terminateAnimation( `Token.${token.id}.animateMovement`);
  targetScene.view();
}

export let setTokenVisibility = async (tokenId: string, visible: boolean) => {
  return requestGMAction(GMAction.actions.setTokenVisibility, {targetSceneId: canvas.scene.id, tokenId, hidden: !visible})
}