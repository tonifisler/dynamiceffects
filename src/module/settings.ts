import { fetchParams } from "./dynamiceffects";

export const registerSettings = function () {
  game.settings.register("dynamiceffects", "ConsumeCharge", {
      name: game.i18n.localize("dynamiceffects.ConsumeCharge.Name"),
      hint: game.i18n.localize("dynamiceffects.ConsumeCharge.Hint"),
      scope: "world",
      default: true,
      config: true,
      type: Boolean
  });
  game.settings.register("dynamiceffects", "requireItemTarget", {
    name: game.i18n.localize("dynamiceffects.requireItemTarget.Name"),
    hint: game.i18n.localize("dynamiceffects.requireItemTarget.Hint"),
    scope: "world",
    default: true,
    config: true,
    type: Boolean,
    onChange: fetchParams
});
};
